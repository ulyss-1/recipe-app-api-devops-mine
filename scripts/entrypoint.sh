#!/bin/sh

set -e      #if any errors - exit script and print errors

python manage.py collectstatic --noinput    #--no-input flag suppresses any user popups with a reply 'yes' and allows to collect static files
python manage.py wait_for_db                #make sure the DB is up
python manage.py migrate                    #migrate changes into DB

uwsgi --socket :9000 --workers 4 --master --enable-threads --module app.wsgi    #uwsgi server runs as TCP service on port 9000. This way we can map the 
                                                                                #requests
                                                                                #from our proxy to the uwsgi service, using uwsgi_path variable, configured
                                                                                #in proxy app
                                                                                #we can run any number of workers depending on number of resources we have
                                                                                #we can have 1 worker and run multiple contaienrs, yet it is better
                                                                                #to run more than one worker per container if possible. Depends if your app
                                                                                #can support it and how much RAM/CPU it requires.
                                                #master flag means run as master-service on terminal if you pass in EXIT signal (docker tries to close)
                                                #it would gracefully close the uWSGI service.
                                                #enable-threads - enables multi-threading in our UWSGI app
                                                #module - is the actual app uwsgi service would run.


