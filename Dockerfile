FROM python:3.7-alpine
LABEL maintainer="London App Developer Ltd"

ENV PYTHONUNBUFFERED 1
#new env. var added
ENV PATH="/scripts:${PATH}"
#added command to install and upgrade pip
RUN pip install --upgrade pip

COPY ./requirements.txt /requirements.txt
RUN apk add --update --no-cache postgresql-client jpeg-dev
RUN apk add --update --no-cache --virtual .tmp-build-deps \
      gcc libc-dev linux-headers postgresql-dev musl-dev zlib zlib-dev
RUN pip install -r /requirements.txt
RUN apk del .tmp-build-deps

RUN mkdir /app
WORKDIR /app
COPY ./app /app
#copy the created entrypoint.sh from folder scritps and allow its' execution
COPY ./scripts/ /scripts/
RUN chmod +x /scripts/*


RUN mkdir -p /vol/web/media
RUN mkdir -p /vol/web/static
RUN adduser -D user
RUN chown -R user:user /vol/
RUN chmod -R 755 /vol/web
USER user

#UPDATE AFTER FARGATE 1.4.0 RELEASE
VOLUME /vol/web
#define default command when our container starts
CMD ["entrypoint.sh"]   