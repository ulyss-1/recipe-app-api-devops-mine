terraform {
  backend "s3" {
    bucket         = "recipe-app-api-devops-tfstate-mine"
    key            = "recipe-app.tfstate"
    region         = "us-east-1"
    encrypt        = true                                  #means state file would be encrypted
    dynamodb_table = "recipe-app-api-devops-tf-state-lock" #this is optional, but strongly suggested!

  }
}

provider "aws" {
  region = "us-east-1" #we need to define it in many places, for the moment it is TF limitation.

  version = "~> 2.50.0" #it is recommended to specify version! as sometimes chanegs may be introduced which would corrupt your config
  #strongly suggested to lock the version, otherwise the deployment may stop working when some change is introdu
  #ced into the newer version of AWS provider (or any other provider), and some feature/function got deprecated
  #we can get the list of version at tf site.
  #- not defined, as they are passed via docker compose from AWS-vault
  #access_key = var.AWS_ACCESS_KEY
  #secret_key = var.AWS_SECRET_KEY 
  #region     = var.AWS_REGION
}

locals {
  prefix = "${var.prefix}-${terraform.workspace}"
  common_tags = {
    Environment = terraform.workspace
    Project     = var.project
    Owner       = var.contact
    ManagedBy   = "Terraform"
  }
}

data "aws_region" "current" {}