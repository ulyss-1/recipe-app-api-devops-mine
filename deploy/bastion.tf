data "aws_ami" "amazon_linux" { #data is for retireving things from AWS
  most_recent = true            #in combination with filtered value - we would ge the most recent image
  filter {
    name   = "name"
    values = ["amzn2-ami-hvm-2.0.*-x86_64-gp2"] #amzn2-ami-hvm-2.0.20210326.0-x86_64-gp2 and we use wildcard to replace the hardcoded part
  }                                             #but it can be a problem as well, as latest build may have issues....
  owners = ["amazon"]
}

resource "aws_iam_role" "bastion" {
  name               = "${local.prefix}-bastion"
  assume_role_policy = file("./templates/bastion/instance-profile-policy.json")

  tags = local.common_tags
}

resource "aws_iam_role_policy_attachment" "bastion_attach_policy" {
  role       = aws_iam_role.bastion.name
  policy_arn = "arn:aws:iam::aws:policy/AmazonEC2ContainerRegistryReadOnly"
}

resource "aws_iam_instance_profile" "bastion" {
  name = "${local.prefix}-bastion-instance-profile"
  role = aws_iam_role.bastion.name
}

resource "aws_instance" "bastion" {
  ami                  = data.aws_ami.amazon_linux.id
  instance_type        = "t2.micro"
  user_data            = file("./templates/bastion/user-data.sh")
  iam_instance_profile = aws_iam_instance_profile.bastion.name
  subnet_id            = aws_subnet.public_a.id
  key_name             = var.bastion_key_name

  vpc_security_group_ids = [
    aws_security_group.bastion.id
  ]

  tags = merge(        #in TF the only way to use common tags but modify one of them is to use merge feature, if we like like before
    local.common_tags, #we'll not be able to set custom name. If you want to override or add one more tag-  you have to use merge feature
    map("Name", "${local.prefix}-bastion")
  )
  #Mark replaced this code with the one above
  #tags = {
  #  Name = "${local.prefix}-bastion" #despite the fact that in main.tf we defines 'locals', inside bastion it is just local. Itsi correct
  #}
}

resource "aws_security_group" "bastion" {
  description = "Control bastion inbound and outbound access"
  name        = "${local.prefix}-bastion"
  vpc_id      = aws_vpc.main.id

  ingress {
    protocol    = "tcp"
    from_port   = 22
    to_port     = 22
    cidr_blocks = ["93.175.229.220/32"]
  }

  egress {
    protocol    = "tcp"
    from_port   = 443
    to_port     = 443
    cidr_blocks = ["0.0.0.0/0"]
  }

  egress {
    protocol    = "tcp"
    from_port   = 80
    to_port     = 80
    cidr_blocks = ["0.0.0.0/0"]
  }

  egress {
    from_port = 5432
    to_port   = 5432
    protocol  = "tcp"
    cidr_blocks = [
      aws_subnet.private_a.cidr_block,
      aws_subnet.private_b.cidr_block,
    ]
  }

  tags = local.common_tags
}