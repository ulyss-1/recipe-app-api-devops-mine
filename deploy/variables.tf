variable "prefix" {
  default = "raadm" #recipe-app-api-devops-mine. This would be prefix of all our resources
}

variable "project" {
  default = "recipe-app-api-devops-mine"
}

variable "contact" {
  default = "email@londonappdev.com"
}

variable "db_username" {
  description = "Username for the RDS Postgres instance"
}

variable "db_password" {
  description = "Password for the RDS postgres instance"
}

variable "bastion_key_name" {
  default = "recipe-app-api-devops-bastion-mine"
}

#this is filled in manually from my ECR - from URI of recipe-app-api-devops-mine
#actually we override the value which is defined in .gitlab-ci.yml in TF_VAR_ecr_image_api=$ECR_REPO:$CI_COMMIT_SHORT_SHA
#with a default one if we ever want to deploy our project from local macine
variable "ecr_image_api" {
  description = "ECR Image for API"
  default     = "295895857794.dkr.ecr.us-east-1.amazonaws.com/recipe-app-api-devops-mine:latest"
}

#this is filled in manually from my ECR - from URI of recipe-app-api-proxy-mine
#Same here: actually we override the value which is defined in .gitlab-ci.yml in TF_VAR_ecr_image_api=$ECR_REPO:$CI_COMMIT_SHORT_SHA
#with a default one if we ever want to deploy our project from local macine
variable "ecr_image_proxy" {
  description = "ECR Image for proxy"
  default     = "295895857794.dkr.ecr.us-east-1.amazonaws.com/recipe-app-api-proxy-mine:latest"
}

#this one we would set via variables in our gitlab project
variable "django_secret_key" {
  description = "Secret key for Django app"
}

#Section 15.3 - add dns name
variable "dns_zone_name" {
  description = "Domain name"
  default     = "securedevops.click"
}

variable "subdomain" {
  description = "Subdomain per environment"
  type        = map(string)
  default = {
    production = "api"
    staging    = "api.staging"
    dev        = "api.dev"
  }
}